# Peltopaikkatieto

Peltojen paikkatietojen hallintaan

1. Asenna Qgis:
https://qgis.org/en/site/forusers/download.html
2. luo "uusi projekti"
3. Lisätään projektiin peruskarttarasteri Kapsin palvelimelta: Tasot - Lisää taso - Lisää WMS-taso
Aukeavasta ikkunasta klikkaa uusi - seuraavaksi Nimi: peruskartta Url: https://tiles.kartat.kapsi.fi/peruskartta?

Nyt klikkaa yhdistä, valitse alle tulevasta listasta rivi joka alkaa 0:lla ja klikkaa Lisää.

Nyt on peruskartta taustaksi. Kannattaa zoomata se johonkin kohtuu pieneen alueeseen, jotta et päädy lataamaan esim koko suomen lohkoja kerralla.

Seuraavaksi lisätään Ruokaviraston dataa:
1. lisää taso - Lisää WFS-taso - Uusi :
Nimi: Ruokavirasto
Url: https://inspire.ruokavirasto-awsa.com/geoserver/wfs?request=getcapabilities
Ok
Yhdistä

2. Listassa on nyt kolme aineistoa joista voit valita kohdassa Abstract on kuvaus mitä kukin sisältää. 
Valitse alin ja: Lisää

Raksuttaa hetken aikaa ja kartalle pitäisi ilmestyä täpliä peruskartan päälle. Nämä ovat peruslohkot. 
Samalla tavalla voi kartalle lisätä kasvulohkot siitä listan ensimmäisestä kohdasta. 

Ikkunan vasemmassa laidassa olevasta Tasot kohdasta voit muuttaa aineistojen näkyvyyttä

kartalla tai niiden järjestystä. Kaksoisklikkaamalla tasoa pääsee muokkaamaan sen ulkoasua, mistä voi muuttaa esim värin peittävyyttä.



Tuo kartta jäi vähän hassun näköiseksi kun jätin käyttöön oletusprojektion. Sen voi muuttaa:
Projekti - ominaisuudet:
Suodatin-kenttään: 3067
Valitse Ennalta määrätyt  koordinaattijärjestelmät luukusta: ETRS89 / TM35FIN ja klikkaa käytä.

